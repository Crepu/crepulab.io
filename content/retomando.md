Title: Retomando
Date: 2023-07-08 19:30
Modified: 2023-07-08 19:30
Category: Test
Tags: testing, blogging
Slug: retomando
Author: Creps
Summary: Después de un rato, nuevo post...
Status: published

# Muchos aaaaños, cuantas lunas?

36 me diría el profe, un amigo. Han pasado exactamente 3 años desde que me senté a escribir el último post de este funesto blog jajaja. Que miserable. Hace algunas semanas estaba revisando mis repositorios de códigos, buscando algún trabajo perdido que hice cuando estaba en la U, y me topé con el repo del blog. Tiene hasta otro nombre, porque me cambié de dominio... En fin, al verlo noté que la fecha de la última publicación había sido hoy pero hace un rato pa atrás. Me propuse firmemente no dejar pasar más de tres años sin subir una entrada nueva y bueno, aqui estoy. Cumpliendome. Importante igual, no relegarse...

Dentro del torrente abrumador que es el internet mismo, a veces cuesta encontrar un oasis donde reposar un rato. Estamos tan atestades de información, se han dado cuenta que por lo menos 1 de cada 5 publicaciones que revisamos en instagram son propagandas? Estoy cunatificando al voleo, pero se entiende la idea. Y no puedo negar tampoco que no caigo en la vorágine del consumismo digital. Entre el correo, el instagram, que se divide en las historias, los reels, las publicaciones -y ahora sacaron otra, threads-, linkedin, porque una se cree profesional dijo la patua, la infinidad de grupos de whatsapp, el telegram, el tuiter, el gogel... ya basta! Y eso que faltan, dije todas las de boomer, pero ahora tenis que estar en el tik-tok pa ser cul o tener un pinterest.

El punto es que a veces se me hace complicado filtrar la información interesante, hay que estar horas haciendo scrol para encontrar algo que valga la pena. Que vaya más allá del mero pasatiempo de revisar un video. Y me hace sentir que me pierdo en montones de información que jamás nunca lograré adquirir, que hay tanto que aprender y tan poco tiempo, pero mientras tanto sigo mirando videos de otras personas. Hay tanto para querer ser que la identidad termina siendo un castillo de arena con vista al mar... En fin, directo a terapia.

Estaba revisando el linkedIn, porque hay que encontrar pega pues, y llegué así medio fortuitamente a un ranking de las mejores universidades de Chile, publicado por Yachay Data. Nada nuevo, la PUC, la Chile y la USACH en los primeros lugares. Sorpresa, la Universidad Adolfo Ibañez en el cuarto lugar y luego más universidades tradicionales y no tanto, la U de Conce, la PUCV, la UTFSM, la UDP y así, le dejo el [post](https://www.linkedin.com/posts/yachaydata_dataviz-yachaydata-information-activity-7080557419956686849-mOwl). Por ahí un tipo que conocí mientras estaba en una de esas Ues que nombro se pegaba la crítica, hay que mostrar que mide el ranking y como lo hacen. Igual cierto, sino cualquier institución que se las pegue de influencer puede andar sacando rankings y la gente termina creyendose el cuento y a propósito de eso, deja otro ranking, el [QS World University Ranking](https://www.topuniversities.com/university-rankings/world-university-rankings/2024). En verdad dejó el link a la metodología del ranking, para no caer en la tentación de decir que los gringos se hicieron un ranking que mida mejor a sus universidades... En fin. Sorpresa otra vez, la UAI también sale cuarta en Chile en ese ranking.

Me llamó la atención, pues no tenía idea, que la UNAM, la Universidad Nacional Autónoma de México, está considerada la segunda mejor universidad Latino Americana, no tan lejos de la Universidad de Sao Paulo. También me llamó la atención, que muy cerquita de la UNAM está la Universidad de Buenos Aires. Pero, no les vine a hablar solo de universidades, también de libros.

Esta introducción es para contarles sobre uno de esos oasis de información que se necesitan cuando se quiere navegar por el internerd. Es un canal de telegram que realmente no estoy bien enterado quien lo administra, probablemente un bot. Llegué a él, por un querido amigo, tejedor de redes como le gusta definirse y tengo la impresión que esta misma persona lo administra. Hay variados temas, de tecnología, ñoñerías musicales como el Theremin, poesía, blogs, seminarios, contrainformación, política y demases. De un cuanto hay como decía mi abuelita. Les dejo el [link](https://t.me/tecnocomunes_seleccion) por si hay interés.

En este canal, vi una foto, esta que les dejo.

![LibrosUnamOpenAccess]({static}/images/librosunamoa.jpg){:width="100%"}

Este es un [epositorio de libros electrónicos de libre acceso](http://www.librosoa.unam.mx/) ublicados por las entidades académicas y dependencias universitarias de la UNAM. Tiene más de 2.000 títulos en diferentes formatos y áreas del conocimiento. Todo esto está incentivando por la Política de Acceso Abierto de la UNAM. Entonces, aparte de ser una de las mejores universidades latinoaméricanas, cualquier libro que se publique bajo su alero, automaticamente tiene libre acceso a cualquier persona que tenga internet. Maravilloso. Además de eso, por si fuera poco, la UNAM tiene el programa de Bachillerato a Distancia. En México, antes de entrar a la Universidad, tienes que hacer la "prepa"(ratoria). Que vendría siendo una especia de enseñanza media o el jai escul gringo (high school). Puedes cursar en poco más de dos años, dedicando 20 horas a la semana, las 24 asignaturas. Completamente online, lo dejo también, ovbia. [Prepa Online UNAM](https://www.bunam.unam.mx/index.php).

Y bueno, viva la dispersión. Hoy aprendí que la UNAM tenía un bachillerato online que pienso cursar, me gusta aprender. Pero no era eso lo que quería mostrarles realmente xd. Tenía un ranking! Ah, no. No es un ranking, pero es una selección de libros interesantes del mismo repositorio que les dejé. Se los muestro en otra ocasión porque este post ya se hizo muy largo. Ahora tengo que acordarme como subirlo ajsadjs... Ya, xao, bai :)
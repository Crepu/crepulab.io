Title: Quién es creps?
Date: 2021-04-08 03:30
Modified: 2021-04-07 21:17
Category: pages
Tags: blog, python
Slug: index
Authors: Creps
Summary: Presentación oficial de quien escribe del otro lado de la pantalla.

Hola, bienvenidx.

Este soy yo. Me llamo Daniel, me dicen Crepu. En instragram soy [tutaykuy](https://www.instagram.com/tutaykuy/){:target="_blank"}.

![Yo]({static}/images/yo.jpg){:width="300px"}
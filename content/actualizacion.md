Title: Actualización
Date: 2020-07-08 21:38
Modified: 2020-07-08 21:28
Category: Test
Tags: testing, learning, documenting
Slug: actualizacion
Author: Creps
Summary: Nueva imagen.
Status: published

# Habemus nueva imagen

Se que quien esté por ahí leyendo no notará ningún cambio, pero de este lado de la pantalla si ocurrieron cosas. Resulta que había instalado Pelican usando el gestor de paquetes de Ubuntu (apt) y por ende estaba usando una versión más antigua del software, la 3 y algo. Generalmente los repositorios del gestor no están en su última versión, se me ocurre que es una medida de seguridad para no usar código que no esté bien revisado por la comunidad y así asegurarse de no dejar el sistema con alguna vulnerabilidad peligrosa para quien lo quiera probar.

Como soy informático, me cambié a la versión stable, la `4.6.0`. Para eso, desintalé la versión antogua y lo volví a descargar. Está vez usé [pip](https://pypi.org/project/pip/), el gestor de paquetes de [Python](https://www.python.org/) que es el lenguaje en el que está escrito Pelican.

Antes de usar pip, me aseguré de crear un entorno virtual para usar el set de herramientas (Pelican otra vez). Para eso, instalé `virtualenvwrapper`. Este paquete me permite administrar mis ambientes y para mantener el orden de los mismos paquetes. Así no confundirlos con los de otros proyectos.

Me llevé la grata sorpresa que al hacer esta subida de nivel la imagen de mi blog cambió a una mucho más bonita y agradable. Que es la que ahora están viendo. Si es que ya no la cambié en el futuro. En fin.

Se pueden usar plantillas para cambiar como se ve todo, incluso se pueden crear plantillas. Pero voy paso a paso. Ahora veré como publicar en internet mis cambios y respaldarlos en algún lado.

XOXO
#!/usr/bin/env python
# -*- coding: utf-8 -*- #

AUTHOR = 'Crepu'
SITENAME = 'Creps Blog'
SITEURL = ''

PATH = 'content'

TIMEZONE = 'America/Santiago'

DEFAULT_LANG = 'es'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (('Pelican', 'https://getpelican.com/'),
         ('Hacker Space Stgo', 'https://www.hackerspace.cl/'),
         ('Gitlab', 'https://gitlab.com/Crepu'),
        )

# Social widget
SOCIAL = (('Mastodon', 'https://chilemasto.casa/@crepu/'),
          ('Pixelfed', 'https://pixel.nobigtech.es/crepu'),
          ('Envs Tilde', 'https://crepu.envs.net/')
)

DEFAULT_PAGINATION = 3

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True
